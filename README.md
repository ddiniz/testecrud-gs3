BACK-END
Para rodar esse projeto você precisa ter um SQL Server rodando de acordo com as configuraçoes no arquivo application.properties desse projeto.
Após configurar o SQL Server, rode o comando "./mvnw spring-boot:run" na pasta principal.
O banco será criado e populado com os dados no arqui src/main/resources/data.sql
Pensei em fazer o CRUD de usuário, mas ia demorar mais tempo ainda, e não era um dos requisitos do projeto.

FRONT-END
Para rodar o front-end, baixe o projeto separadamente no gitlab "testecrud-gs3-front" e rode o comando "npm install" e depois "npm start".

Se tudo der certo, seu back-end deve estar rodando em localhost:8080 e o front em localhost:3000. O CORS está configurado para estes endereços.
