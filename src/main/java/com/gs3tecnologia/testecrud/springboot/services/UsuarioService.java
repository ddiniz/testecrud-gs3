package com.gs3tecnologia.testecrud.springboot.services;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.gs3tecnologia.testecrud.springboot.dto.UsuarioDTO;
import com.gs3tecnologia.testecrud.springboot.entities.Usuario;
import com.gs3tecnologia.testecrud.springboot.persistence.UsuarioRepository;
import com.gs3tecnologia.testecrud.springboot.security.Encryptor;
import com.gs3tecnologia.testecrud.springboot.utils.exceptions.GS3Exception;
import com.gs3tecnologia.testecrud.springboot.utils.exceptions.rest.UsuarioInexistenteException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService {
    @Autowired
    private UsuarioRepository repository;

    public UsuarioDTO save(UsuarioDTO obj) throws GS3Exception {
        Usuario novo = obj.toEntity();
        repository.save(novo);
        return new UsuarioDTO(novo);
    }

    public UsuarioDTO update(UsuarioDTO obj) throws GS3Exception {
        Usuario usuario = getUsuario(obj.getId());
        usuario.setPassword(Encryptor.encrypt(obj.getPassword()));
        usuario.setUsername(obj.getUsername());
        usuario.setPermissao(obj.getPermissao());
        repository.save(usuario);
        return new UsuarioDTO(usuario);
    }

    public UsuarioDTO find(long id) throws GS3Exception {
        Usuario usuario = getUsuario(id);
        return new UsuarioDTO(usuario);
    }

    public Set<UsuarioDTO> list() throws GS3Exception {
        return new HashSet<>(repository.findAll().stream().map(x -> new UsuarioDTO(x)).collect(Collectors.toList()));
    }

    public void delete(long id) throws GS3Exception {
        Usuario usuario = getUsuario(id);
        repository.delete(usuario);
    }

    private Usuario getUsuario(long id) throws GS3Exception {
        Optional<Usuario> usuarioOpt = repository.findById(id);
        if (!usuarioOpt.isPresent()) {
            throw new UsuarioInexistenteException();
        } else {
            return usuarioOpt.get();
        }
    }
}
