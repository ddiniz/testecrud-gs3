package com.gs3tecnologia.testecrud.springboot.services;

import java.util.Optional;

import com.gs3tecnologia.testecrud.springboot.dto.LoginDTO;
import com.gs3tecnologia.testecrud.springboot.entities.Usuario;
import com.gs3tecnologia.testecrud.springboot.persistence.UsuarioRepository;
import com.gs3tecnologia.testecrud.springboot.security.Encryptor;
import com.gs3tecnologia.testecrud.springboot.utils.exceptions.GS3Exception;
import com.gs3tecnologia.testecrud.springboot.utils.exceptions.rest.LoginInvalidoException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthService {

    @Autowired
    private UsuarioRepository repository;

    public Usuario login(LoginDTO login) throws GS3Exception {
        Optional<Usuario> usuarioOpt = repository.findByUsername(login.getUsername());
        if (!usuarioOpt.isPresent())
            throw new LoginInvalidoException();

        Usuario usuario = usuarioOpt.get();
        if (!Encryptor.compare(login.getPassword(), usuario.getPassword()))
            throw new LoginInvalidoException();

        return usuario;
    }

    public void logoff() throws GS3Exception {
    }

}
