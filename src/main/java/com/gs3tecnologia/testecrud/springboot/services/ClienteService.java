package com.gs3tecnologia.testecrud.springboot.services;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import com.gs3tecnologia.testecrud.springboot.dto.ClienteDTO;
import com.gs3tecnologia.testecrud.springboot.dto.EmailDTO;
import com.gs3tecnologia.testecrud.springboot.dto.EnderecoDTO;
import com.gs3tecnologia.testecrud.springboot.dto.TelefoneDTO;
import com.gs3tecnologia.testecrud.springboot.entities.Cliente;
import com.gs3tecnologia.testecrud.springboot.entities.Email;
import com.gs3tecnologia.testecrud.springboot.entities.Endereco;
import com.gs3tecnologia.testecrud.springboot.entities.Telefone;
import com.gs3tecnologia.testecrud.springboot.persistence.ClienteRepository;
import com.gs3tecnologia.testecrud.springboot.persistence.EmailRepository;
import com.gs3tecnologia.testecrud.springboot.persistence.TelefoneRepository;
import com.gs3tecnologia.testecrud.springboot.utils.exceptions.GS3Exception;
import com.gs3tecnologia.testecrud.springboot.utils.exceptions.rest.ClienteInexistenteException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository repository;
    @Autowired
    private TelefoneRepository repositoryTelefone;
    @Autowired
    private EmailRepository repositoryEmail;

    public ClienteDTO save(ClienteDTO obj) throws GS3Exception {
        Cliente novo = obj.toEntity();
        Set<Email> emails = obj.getEmails().stream().map(x -> {
            Email email = x.toEntity();
            email.setCliente(novo);
            return email;
        }).collect(Collectors.toSet());

        Set<Telefone> telefones = obj.getTelefones().stream().map(x -> {
            Telefone tel = x.toEntity();
            tel.setCliente(novo);
            return tel;
        }).collect(Collectors.toSet());

        Endereco endereco = obj.getEndereco().toEntity();
        endereco.setCliente(novo);
        novo.setEmails(emails);
        novo.setTelefones(telefones);
        novo.setEndereco(endereco);
        repository.save(novo);
        return new ClienteDTO(novo);
    }

    @Transactional
    public ClienteDTO update(ClienteDTO obj) throws GS3Exception {
        getCliente(obj.getId());

        Cliente cliente = repository.findWithEntities(obj.getId());

        atualizarEmails(obj, cliente);

        atualizarTelefones(obj, cliente);
        cliente.setCpf(obj.getCpf());
        cliente.setNome(obj.getNome());
        cliente.setEndereco(cliente.getEndereco().update(obj.getEndereco()));
        repository.save(cliente);
        return new ClienteDTO(cliente);
    }

    @Transactional
    private void atualizarTelefones(ClienteDTO obj, Cliente cliente) {
        Set<TelefoneDTO> telefonesNovos = obj.getTelefones()
                                             .stream()
                                             .filter(x -> x.getId() == 0)
                                             .collect(Collectors.toSet());

        // separando telefones mantidos dos removidos
        Map<Long, TelefoneDTO> mantidosDtoMap = new HashMap<>();
        Set<Long> idsTelefonesMantidos = new HashSet<>();
        for (TelefoneDTO telefoneDto : obj.getTelefones()) {
            if (telefoneDto.getId() != 0) {
                idsTelefonesMantidos.add(telefoneDto.getId());
                mantidosDtoMap.put(telefoneDto.getId(), telefoneDto);// guardando num hashmap pra facilitar na hora de
                // atualizar valores
            }
        }
        Set<Long> idsTelefonesAntigos = cliente.getTelefones().stream().map(x -> x.getId()).collect(Collectors.toSet());
        Set<Long> idsTelefonesRemovidos = new HashSet<>(idsTelefonesAntigos);
        idsTelefonesRemovidos.removeAll(idsTelefonesMantidos);

        // removendo telefones e adicionando mantidos
        Set<Telefone> mantidosTelEnt = new HashSet<>();
        for (Telefone telefone : cliente.getTelefones()) {
            if (idsTelefonesRemovidos.contains(telefone.getId()))
                repositoryTelefone.delete(telefone.getId());
            if (idsTelefonesMantidos.contains(telefone.getId()))
                mantidosTelEnt.add(telefone);
        }
        for (Telefone telefone : mantidosTelEnt) {
            if (mantidosDtoMap.containsKey(telefone.getId())) {
                telefone.update(mantidosDtoMap.get(telefone.getId()));
            }
        }
        cliente.setTelefones(mantidosTelEnt);

        // adicionando todos os telefones novos
        cliente.getTelefones().addAll(telefonesNovos.stream().map(x -> {
            Telefone telefone = x.toEntity();
            telefone.setCliente(cliente);
            repositoryTelefone.save(telefone);
            return telefone;
        }).collect(Collectors.toSet()));
    }

    @Transactional
    private void atualizarEmails(ClienteDTO obj, Cliente cliente) {
        Set<EmailDTO> emailsNovos = obj.getEmails().stream().filter(x -> x.getId() == 0).collect(Collectors.toSet());

        // separando emails mantidos dos removidos
        Map<Long, EmailDTO> mantidosDtoMap = new HashMap<>();
        Set<Long> idsEmailsMantidos = new HashSet<>();
        for (EmailDTO emailDto : obj.getEmails()) {
            if (emailDto.getId() != 0) {
                idsEmailsMantidos.add(emailDto.getId());
                mantidosDtoMap.put(emailDto.getId(), emailDto);// guardando num hashmap pra facilitar na hora de
                                                               // atualizar valores
            }
        }
        Set<Long> idsEmailsAntigos = cliente.getEmails().stream().map(x -> x.getId()).collect(Collectors.toSet());
        Set<Long> idsEmailsRemovidos = new HashSet<>(idsEmailsAntigos);
        idsEmailsRemovidos.removeAll(idsEmailsMantidos);

        // removendo emails e adicionando mantidos
        Set<Email> mantidosEmailEnt = new HashSet<>();
        for (Email email : cliente.getEmails()) {
            if (idsEmailsRemovidos.contains(email.getId()))
                repositoryEmail.delete(email.getId());
            if (idsEmailsMantidos.contains(email.getId()))
                mantidosEmailEnt.add(email);
        }
        for (Email email : mantidosEmailEnt) {
            if (mantidosDtoMap.containsKey(email.getId())) {
                email.update(mantidosDtoMap.get(email.getId()));
            }
        }
        cliente.setEmails(mantidosEmailEnt);

        // adicionando todos os emails novos
        cliente.getEmails().addAll(emailsNovos.stream().map(x -> {
            Email email = x.toEntity();
            email.setCliente(cliente);
            repositoryEmail.save(email);
            return email;
        }).collect(Collectors.toSet()));
    }

    public ClienteDTO find(long id) throws GS3Exception {
        getCliente(id);
        Cliente cliente = repository.findWithEntities(id);
        List<EmailDTO> emails = cliente.getEmails().stream().map(x -> new EmailDTO(x)).collect(Collectors.toList());

        List<TelefoneDTO> telefones = cliente.getTelefones()
                                             .stream()
                                             .map(x -> new TelefoneDTO(x))
                                             .collect(Collectors.toList());
        ClienteDTO res = new ClienteDTO(cliente);
        res.setEmails(emails);
        res.setTelefones(telefones);
        res.setEndereco(new EnderecoDTO(cliente.getEndereco()));
        return res;
    }

    public Set<ClienteDTO> list() throws GS3Exception {
        return new HashSet<>(repository.findAll().stream().map(x -> new ClienteDTO(x)).collect(Collectors.toList()));
    }

    @Transactional
    public void delete(long id) throws GS3Exception {
        Cliente cliente = getCliente(id);
        repository.delete(cliente);
    }

    private Cliente getCliente(long id) throws GS3Exception {
        Optional<Cliente> clienteOpt = repository.findById(id);
        if (!clienteOpt.isPresent()) {
            throw new ClienteInexistenteException();
        } else {
            return clienteOpt.get();
        }
    }

}
