package com.gs3tecnologia.testecrud.springboot.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.NUMBER)
public enum TipoTelefone {
    RESIDENCIAL(0),
    CELULAR(1),
    COMERCIAL(2);

    final int val;

    TipoTelefone(int val) {
        this.val = val;
    }
}
