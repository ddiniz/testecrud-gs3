package com.gs3tecnologia.testecrud.springboot.rest;

import java.util.Set;

import javax.validation.Valid;

import com.gs3tecnologia.testecrud.springboot.dto.ClienteDTO;
import com.gs3tecnologia.testecrud.springboot.services.ClienteService;
import com.gs3tecnologia.testecrud.springboot.utils.exceptions.GS3Exception;
import com.gs3tecnologia.testecrud.springboot.utils.exceptions.rest.SemPermissaoException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestCliente extends SuperRest {
    @Autowired
    ClienteService service;

    @RequestMapping(value = "/cliente/hello", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> hello() {
        return okPadrao();
    }

    @PostMapping(value = "/cliente", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> save(@RequestBody @Valid ClienteDTO obj) {
        try {
            if (!isAdmin())
                throw new SemPermissaoException();
            ClienteDTO res = service.save(obj);
            return okPadrao(res);
        } catch (GS3Exception e) {
            return erroPadrao(e);
        }

    }

    @Transactional
    @PutMapping(value = "/cliente", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> update(@RequestBody @Valid ClienteDTO obj) {
        try {
            if (!isAdmin())
                throw new SemPermissaoException();
            ClienteDTO res = service.update(obj);
            return okPadrao(res);
        } catch (GS3Exception e) {
            return erroPadrao(e);
        }
    }

    @GetMapping(value = "/cliente/{id}", produces = "application/json")
    public ResponseEntity<Object> find(@PathVariable long id) {
        try {
            ClienteDTO res = service.find(id);
            return okPadrao(res);
        } catch (GS3Exception e) {
            return erroPadrao(e);
        }
    }

    @GetMapping(value = "/cliente/list", produces = "application/json")
    public ResponseEntity<Object> list() {
        try {
            Set<ClienteDTO> list = service.list();
            return okPadrao(list);
        } catch (GS3Exception e) {
            return erroPadrao(e);
        }
    }

    @Transactional
    @DeleteMapping(value = "/cliente/{id}", produces = "application/json")
    public ResponseEntity<Object> delete(@PathVariable long id) {
        try {
            if (!isAdmin())
                throw new SemPermissaoException();
            service.delete(id);
            return okPadrao();
        } catch (GS3Exception e) {
            return erroPadrao(e);
        }
    }

}
