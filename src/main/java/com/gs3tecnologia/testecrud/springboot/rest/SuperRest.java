package com.gs3tecnologia.testecrud.springboot.rest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gs3tecnologia.testecrud.springboot.enums.Permissao;
import com.gs3tecnologia.testecrud.springboot.security.JwtToken;
import com.gs3tecnologia.testecrud.springboot.utils.exceptions.GS3Exception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

public class SuperRest {
    @Autowired
    HttpServletRequest request;
    @Autowired
    HttpServletResponse response;

    protected ResponseEntity<Object> okPadrao() {
        return ResponseEntity.ok().build();
    }

    protected ResponseEntity<Object> okPadrao(Object o) {
        return ResponseEntity.ok(o);
    }

    protected ResponseEntity<Object> erroPadrao(GS3Exception e) {
        e.printStackTrace();
        return ResponseEntity.status(e.getStatus()).body(e.getMessage());
    }

    protected boolean isAdmin() throws GS3Exception {
        return JwtToken.getPermissaoUsuario(request).equals(Permissao.ADMIN);
    }
}
