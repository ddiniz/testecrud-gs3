package com.gs3tecnologia.testecrud.springboot.rest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gs3tecnologia.testecrud.springboot.dto.CepDTO;

@RestController
public class RestExterno extends SuperRest {

	static ObjectMapper objectMapper = new ObjectMapper();
	static final String CHARSET = "UTF-8";

	@GetMapping(value = "/cep/{cep}")
	public ResponseEntity<Object> cep(@PathVariable String cep) {
		try {

			URL url = new URL("https://viacep.com.br/ws/" + cep.trim().replace("-", "") + "/json/");
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setDoInput(true);
			connection.setRequestProperty("Accept-Charset", CHARSET);
			connection.addRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("Accept", "application/json");

			int resultHttp = connection.getResponseCode();
			if (resultHttp == HttpURLConnection.HTTP_OK) {
				CepDTO res = objectMapper.readValue(connection.getInputStream(), CepDTO.class);
				return okPadrao(res);
			} else {
				StringBuilder sb = new StringBuilder();
				BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), CHARSET));
				String line;
				while ((line = br.readLine()) != null) {
					sb.append(line + "\n");
				}
				br.close();
				throw new Exception(resultHttp + " " + sb.toString());
			}
		} catch (final Exception e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
		}
	}

}
