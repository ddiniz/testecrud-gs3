package com.gs3tecnologia.testecrud.springboot.rest;

import java.util.Set;

import com.gs3tecnologia.testecrud.springboot.dto.UsuarioDTO;
import com.gs3tecnologia.testecrud.springboot.services.UsuarioService;
import com.gs3tecnologia.testecrud.springboot.utils.exceptions.GS3Exception;
import com.gs3tecnologia.testecrud.springboot.utils.exceptions.rest.SemPermissaoException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestUsuario extends SuperRest {
    @Autowired
    UsuarioService service;

    @RequestMapping(value = "/usuario/hello", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> hello() {
        return okPadrao();
    }

    @PostMapping(value = "/usuario", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> save(@RequestBody UsuarioDTO obj) {
        try {
            if (!isAdmin())
                throw new SemPermissaoException();
            UsuarioDTO res = service.save(obj);
            return okPadrao(res);
        } catch (GS3Exception e) {
            return erroPadrao(e);
        }
    }

    @PutMapping(value = "/usuario", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> update(@RequestBody UsuarioDTO obj) {
        try {
            if (!isAdmin())
                throw new SemPermissaoException();
            UsuarioDTO res = service.update(obj);
            return okPadrao(res);
        } catch (GS3Exception e) {
            return erroPadrao(e);
        }
    }

    @GetMapping(value = "/usuario/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> find(@PathVariable long id) {
        try {
            if (!isAdmin())
                throw new SemPermissaoException();
            UsuarioDTO res = service.find(id);
            return okPadrao(res);
        } catch (GS3Exception e) {
            return erroPadrao(e);
        }
    }

    @GetMapping(value = "/usuario/list", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> list() {
        try {
            if (!isAdmin())
                throw new SemPermissaoException();
            Set<UsuarioDTO> list = service.list();
            return okPadrao(list);
        } catch (GS3Exception e) {
            return erroPadrao(e);
        }
    }

    @DeleteMapping(value = "/usuario/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> delete(@PathVariable long id) {
        try {
            if (!isAdmin())
                throw new SemPermissaoException();
            service.delete(id);
            return okPadrao();
        } catch (GS3Exception e) {
            return erroPadrao(e);
        }
    }
}
