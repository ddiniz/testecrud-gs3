package com.gs3tecnologia.testecrud.springboot.rest;

import com.gs3tecnologia.testecrud.springboot.dto.LoginDTO;
import com.gs3tecnologia.testecrud.springboot.entities.Usuario;
import com.gs3tecnologia.testecrud.springboot.security.JwtToken;
import com.gs3tecnologia.testecrud.springboot.services.AuthService;
import com.gs3tecnologia.testecrud.springboot.utils.exceptions.GS3Exception;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestAuth extends SuperRest {
    @Autowired
    AuthService service;

    @GetMapping(value = "/auth/hello")
    public ResponseEntity<Object> hello() {
        return okPadrao();
    }

    @PostMapping(value = "/auth/login", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> login(@RequestBody LoginDTO login) {
        try {
            Usuario usuarioLogado = service.login(login);
            String token = JwtToken.addAuthentication(response, usuarioLogado);
            return okPadrao(token);
        } catch (GS3Exception e) {
            return erroPadrao(e);
        }
    }

    @GetMapping(value = "/auth/logoff", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> logoff() {
        return okPadrao();
    }
}
