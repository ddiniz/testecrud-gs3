package com.gs3tecnologia.testecrud.springboot.persistence;

import com.gs3tecnologia.testecrud.springboot.entities.Telefone;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TelefoneRepository extends JpaRepository<Telefone, Long> {
    @Modifying
    @Query("delete from Telefone t where t.id = ?1")
    void delete(Long entityId);
}
