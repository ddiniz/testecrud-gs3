package com.gs3tecnologia.testecrud.springboot.persistence;

import com.gs3tecnologia.testecrud.springboot.entities.Endereco;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnderecoRepository extends JpaRepository<Endereco, Long> {

}
