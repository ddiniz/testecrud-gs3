package com.gs3tecnologia.testecrud.springboot.persistence;

import com.gs3tecnologia.testecrud.springboot.entities.Email;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface EmailRepository extends JpaRepository<Email, Long> {
    @Modifying
    @Query("delete from Email e where e.id = ?1")
    void delete(Long entityId);
}
