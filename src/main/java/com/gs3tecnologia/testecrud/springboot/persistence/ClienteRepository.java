package com.gs3tecnologia.testecrud.springboot.persistence;

import com.gs3tecnologia.testecrud.springboot.entities.Cliente;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {
    @Query("select c from Cliente c "
            + " join fetch c.telefones t "
            + " join fetch c.emails e "
            + " join fetch c.endereco en "
            + " where c.id = ?1 ")
    Cliente findWithEntities(long id);
}
