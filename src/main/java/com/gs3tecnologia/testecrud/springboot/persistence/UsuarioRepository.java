package com.gs3tecnologia.testecrud.springboot.persistence;

import java.util.Optional;

import com.gs3tecnologia.testecrud.springboot.entities.Usuario;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
    Optional<Usuario> findByUsername(String username);
}
