package com.gs3tecnologia.testecrud.springboot.dto;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gs3tecnologia.testecrud.springboot.entities.Cliente;
import com.gs3tecnologia.testecrud.springboot.utils.deserializers.CPFDeserializer;
import com.gs3tecnologia.testecrud.springboot.utils.deserializers.CPFSerializer;

import org.hibernate.validator.constraints.br.CPF;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClienteDTO {
    long id;

    @NotBlank(message = "Nome não pode ser vazio")
    @Size(min = 3, max = 100)
    String nome;

    @NotBlank(message = "CPF não pode ser vazio")
    @JsonDeserialize(using = CPFDeserializer.class)
    @JsonSerialize(using = CPFSerializer.class)
    @CPF
    String cpf;

    List<EmailDTO> emails;
    List<TelefoneDTO> telefones;
    EnderecoDTO endereco;

    public ClienteDTO() {
        super();
    }

    public ClienteDTO(Cliente ent) {
        this.id = ent.getId();
        this.nome = ent.getNome();
        this.cpf = ent.getCpf();
    }

    public Cliente toEntity() {
        return new Cliente().setCpf(this.cpf).setNome(this.nome);
    }

    public long getId() {
        return id;
    }

    public ClienteDTO setId(long id) {
        this.id = id;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public ClienteDTO setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public String getCpf() {
        return cpf;
    }

    public ClienteDTO setCpf(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public List<EmailDTO> getEmails() {
        return emails;
    }

    public ClienteDTO setEmails(List<EmailDTO> emails) {
        this.emails = emails;
        return this;
    }

    public List<TelefoneDTO> getTelefones() {
        return telefones;
    }

    public ClienteDTO setTelefones(List<TelefoneDTO> telefones) {
        this.telefones = telefones;
        return this;
    }

    public EnderecoDTO getEndereco() {
        return endereco;
    }

    public ClienteDTO setEndereco(EnderecoDTO endereco) {
        this.endereco = endereco;
        return this;
    }

}
