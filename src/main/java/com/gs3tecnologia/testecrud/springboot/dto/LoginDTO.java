package com.gs3tecnologia.testecrud.springboot.dto;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginDTO {
    @NotBlank(message = "Usuário não pode ser vazio")
    String username;

    @NotBlank(message = "Senha não pode ser vazia")
    String password;

    public LoginDTO() {
        super();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
