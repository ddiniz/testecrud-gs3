package com.gs3tecnologia.testecrud.springboot.dto;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gs3tecnologia.testecrud.springboot.entities.Telefone;
import com.gs3tecnologia.testecrud.springboot.enums.TipoTelefone;
import com.gs3tecnologia.testecrud.springboot.utils.deserializers.TelefoneDeserializer;
import com.gs3tecnologia.testecrud.springboot.utils.deserializers.TelefoneSerializer;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TelefoneDTO {
    long id;

    @NotBlank(message = "Telefone não pode ser vazio")
    @JsonDeserialize(using = TelefoneDeserializer.class)
    @JsonSerialize(using = TelefoneSerializer.class)
    String telefone;

    TipoTelefone tipo;

    ClienteDTO cliente;

    public TelefoneDTO() {
        super();
    }

    public TelefoneDTO(Telefone ent) {
        this.id = ent.getId();
        this.telefone = ent.getTelefone();
        this.tipo = ent.getTipo();
    }

    public Telefone toEntity() {
        return new Telefone().setTelefone(this.telefone).setTipo(this.tipo);
    }

    public long getId() {
        return id;
    }

    public TelefoneDTO setId(long id) {
        this.id = id;
        return this;
    }

    public String getTelefone() {
        return telefone;
    }

    public TelefoneDTO setTelefone(String telefone) {
        this.telefone = telefone;
        return this;
    }

    public TipoTelefone getTipo() {
        return tipo;
    }

    public TelefoneDTO setTipo(TipoTelefone tipo) {
        this.tipo = tipo;
        return this;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public TelefoneDTO setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
        return this;
    }

}
