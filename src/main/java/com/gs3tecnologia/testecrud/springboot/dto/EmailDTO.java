package com.gs3tecnologia.testecrud.springboot.dto;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gs3tecnologia.testecrud.springboot.entities.Email;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailDTO {
    long id;

    @NotBlank(message = "E-mail não pode ser vazio")
    @javax.validation.constraints.Email(message = "E-mail deve ser válido.")
    String email;

    ClienteDTO cliente;

    public EmailDTO() {
        super();
    }

    public EmailDTO(Email ent) {
        this.id = ent.getId();
        this.email = ent.getEmail();
    }

    public Email toEntity() {
        return new Email().setEmail(this.email);
    }

    public long getId() {
        return id;
    }

    public EmailDTO setId(long id) {
        this.id = id;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public EmailDTO setEmail(String email) {
        this.email = email;
        return this;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public EmailDTO setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
        return this;
    }

}
