package com.gs3tecnologia.testecrud.springboot.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gs3tecnologia.testecrud.springboot.entities.Endereco;
import com.gs3tecnologia.testecrud.springboot.utils.deserializers.CEPDeserializer;
import com.gs3tecnologia.testecrud.springboot.utils.deserializers.CEPSerializer;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EnderecoDTO {
    long id;

    @NotBlank(message = "CEP não pode ser vazio")
    @Min(value = 9, message = "CEP inválido")
    @JsonDeserialize(using = CEPDeserializer.class)
    @JsonSerialize(using = CEPSerializer.class)
    String cep;

    @NotBlank(message = "Logradouro não pode ser vazio")
    String logradouro;

    @NotBlank(message = "Bairro não pode ser vazio")
    String bairro;

    @NotBlank(message = "Cidade não pode ser vazia")
    String cidade;

    String complemento;

    String numero;

    @NotBlank(message = "UF não pode ser vazia")
    String uf;

    ClienteDTO cliente;

    public EnderecoDTO() {
        super();
    }

    public EnderecoDTO(Endereco ent) {
        this.id = ent.getId();
        this.cep = ent.getCep();
        this.logradouro = ent.getLogradouro();
        this.bairro = ent.getBairro();
        this.cidade = ent.getCidade();
        this.complemento = ent.getComplemento();
        this.numero = ent.getNumero();
        this.uf = ent.getUf();
    }

    public Endereco toEntity() {
        return new Endereco().setCep(this.cep)
                             .setLogradouro(this.logradouro)
                             .setBairro(this.bairro)
                             .setCidade(this.cidade)
                             .setComplemento(this.complemento)
                             .setNumero(this.numero)
                             .setUf(this.uf);
    }

    public long getId() {
        return id;
    }

    public EnderecoDTO setId(long id) {
        this.id = id;
        return this;
    }

    public String getCep() {
        return cep;
    }

    public EnderecoDTO setCep(String cep) {
        this.cep = cep;
        return this;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public EnderecoDTO setLogradouro(String logradouro) {
        this.logradouro = logradouro;
        return this;
    }

    public String getBairro() {
        return bairro;
    }

    public EnderecoDTO setBairro(String bairro) {
        this.bairro = bairro;
        return this;
    }

    public String getCidade() {
        return cidade;
    }

    public EnderecoDTO setCidade(String cidade) {
        this.cidade = cidade;
        return this;
    }

    public String getComplemento() {
        return complemento;
    }

    public EnderecoDTO setComplemento(String complemento) {
        this.complemento = complemento;
        return this;
    }

    public String getNumero() {
        return numero;
    }

    public EnderecoDTO setNumero(String numero) {
        this.numero = numero;
        return this;
    }

    public String getUf() {
        return uf;
    }

    public EnderecoDTO setUf(String uf) {
        this.uf = uf;
        return this;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public EnderecoDTO setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
        return this;
    }

}
