package com.gs3tecnologia.testecrud.springboot.dto;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.gs3tecnologia.testecrud.springboot.entities.Usuario;
import com.gs3tecnologia.testecrud.springboot.enums.Permissao;
import com.gs3tecnologia.testecrud.springboot.security.Encryptor;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UsuarioDTO {
    long id;
    @NotBlank(message = "Login não pode ser vazio")
    String username;
    @NotBlank(message = "Senha não pode ser vazia")
    String password;
    Permissao permissao;

    public UsuarioDTO() {
        super();
    }

    public UsuarioDTO(Usuario ent) {
        this.id = ent.getId();
        this.username = ent.getUsername();
        this.password = ent.getPassword();
        this.permissao = ent.getPermissao();
    }

    public Usuario toEntity() {
        return new Usuario().setUsername(this.username)
                            .setPassword(Encryptor.encrypt(this.password))
                            .setPermissao(this.permissao);
    }

    public long getId() {
        return id;
    }

    public UsuarioDTO setId(long id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public UsuarioDTO setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UsuarioDTO setPassword(String password) {
        this.password = password;
        return this;
    }

    public Permissao getPermissao() {
        return permissao;
    }

    public UsuarioDTO setPermissao(Permissao permissao) {
        this.permissao = permissao;
        return this;
    }

}
