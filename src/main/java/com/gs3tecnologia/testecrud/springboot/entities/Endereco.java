package com.gs3tecnologia.testecrud.springboot.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.gs3tecnologia.testecrud.springboot.dto.EnderecoDTO;

@Entity
@Table(name = "ENDERECO")
public class Endereco {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    long id;
    @Column(name = "DS_CEP")
    String cep;
    @Column(name = "DS_LOGRADOURO")
    String logradouro;
    @Column(name = "DS_BAIRRO")
    String bairro;
    @Column(name = "DS_CIDADE")
    String cidade;
    @Column(name = "DS_COMPLEMENTO")
    String complemento;
    @Column(name = "DS_NUMERO")
    String numero;
    @Column(name = "DS_UF")
    String uf;

    @OneToOne
    @JoinColumn(name = "FK_ENDERECO_CLIENTE", foreignKey = @ForeignKey(name = "FK_CONSTRAINT_ENDERECO_CLIENTE"))
    Cliente cliente;

    public Endereco() {
        super();
    }

    public Endereco update(EnderecoDTO endereco) {
        this.cep = endereco.getCep();
        this.logradouro = endereco.getLogradouro();
        this.bairro = endereco.getBairro();
        this.cidade = endereco.getCidade();
        this.complemento = endereco.getComplemento();
        this.numero = endereco.getNumero();
        this.uf = endereco.getUf();
        return this;
    }

    public long getId() {
        return id;
    }

    public Endereco setId(long id) {
        this.id = id;
        return this;
    }

    public String getCep() {
        return cep;
    }

    public Endereco setCep(String cep) {
        this.cep = cep;
        return this;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public Endereco setLogradouro(String logradouro) {
        this.logradouro = logradouro;
        return this;
    }

    public String getBairro() {
        return bairro;
    }

    public Endereco setBairro(String bairro) {
        this.bairro = bairro;
        return this;
    }

    public String getCidade() {
        return cidade;
    }

    public Endereco setCidade(String cidade) {
        this.cidade = cidade;
        return this;
    }

    public String getComplemento() {
        return complemento;
    }

    public Endereco setComplemento(String complemento) {
        this.complemento = complemento;
        return this;
    }

    public String getNumero() {
        return numero;
    }

    public Endereco setNumero(String numero) {
        this.numero = numero;
        return this;
    }

    public String getUf() {
        return uf;
    }

    public Endereco setUf(String uf) {
        this.uf = uf;
        return this;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Endereco setCliente(Cliente cliente) {
        this.cliente = cliente;
        return this;
    }

}
