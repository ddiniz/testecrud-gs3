package com.gs3tecnologia.testecrud.springboot.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.gs3tecnologia.testecrud.springboot.enums.Permissao;

@Entity
@Table(name = "USUARIO")
public class Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    long id;
    @Column(name = "DS_USERNAME")
    String username;
    @Column(name = "DS_PASSWORD")
    String password;
    @Column(name = "TP_PERMISSAO")
    Permissao permissao;

    public Usuario() {
        super();
    }

    public long getId() {
        return id;
    }

    public Usuario setId(long id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public Usuario setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public Usuario setPassword(String password) {
        this.password = password;
        return this;
    }

    public Permissao getPermissao() {
        return permissao;
    }

    public Usuario setPermissao(Permissao permissao) {
        this.permissao = permissao;
        return this;
    }

}
