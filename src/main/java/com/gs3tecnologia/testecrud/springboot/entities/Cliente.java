package com.gs3tecnologia.testecrud.springboot.entities;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CLIENTE")
public class Cliente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    long id;
    @Column(name = "DS_NOME")
    String nome;
    @Column(name = "DS_CPF")
    String cpf;

    @OneToMany(mappedBy = "cliente", cascade = { CascadeType.PERSIST, CascadeType.REMOVE })
    Set<Email> emails;

    @OneToMany(mappedBy = "cliente", cascade = { CascadeType.PERSIST, CascadeType.REMOVE })
    Set<Telefone> telefones;

    @OneToOne(mappedBy = "cliente", cascade = { CascadeType.PERSIST, CascadeType.REMOVE })
    Endereco endereco;

    public Cliente() {
        super();
    }

    public long getId() {
        return id;
    }

    public Cliente setId(long id) {
        this.id = id;
        return this;
    }

    public String getNome() {
        return nome;
    }

    public Cliente setNome(String nome) {
        this.nome = nome;
        return this;
    }

    public String getCpf() {
        return cpf;
    }

    public Cliente setCpf(String cpf) {
        this.cpf = cpf;
        return this;
    }

    public Set<Email> getEmails() {
        return emails;
    }

    public Cliente setEmails(Set<Email> emails) {
        this.emails = emails;
        return this;
    }

    public Set<Telefone> getTelefones() {
        return telefones;
    }

    public Cliente setTelefones(Set<Telefone> telefones) {
        this.telefones = telefones;
        return this;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public Cliente setEndereco(Endereco endereco) {
        this.endereco = endereco;
        return this;
    }

}
