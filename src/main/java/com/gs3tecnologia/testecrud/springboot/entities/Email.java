package com.gs3tecnologia.testecrud.springboot.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.gs3tecnologia.testecrud.springboot.dto.EmailDTO;

@Entity
@Table(name = "EMAIL")
public class Email {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    long id;
    @Column(name = "DS_EMAIL")
    String email;

    @ManyToOne
    @JoinColumn(name = "FK_EMAIL_CLIENTE", foreignKey = @ForeignKey(name = "FK_CONSTRAINT_EMAIL_CLIENTE"))
    Cliente cliente;

    public Email() {
        super();
    }

    public Email update(EmailDTO emailDTO) {
        this.email = emailDTO.getEmail();
        return this;
    }

    public long getId() {
        return id;
    }

    public Email setId(long id) {
        this.id = id;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Email setEmail(String email) {
        this.email = email;
        return this;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Email setCliente(Cliente cliente) {
        this.cliente = cliente;
        return this;
    }

}
