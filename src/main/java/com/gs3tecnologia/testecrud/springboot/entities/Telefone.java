package com.gs3tecnologia.testecrud.springboot.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.gs3tecnologia.testecrud.springboot.dto.TelefoneDTO;
import com.gs3tecnologia.testecrud.springboot.enums.TipoTelefone;

@Entity
@Table(name = "TELEFONE")
public class Telefone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    long id;
    @Column(name = "DS_TELEFONE")
    String telefone;
    @Column(name = "TP_TIPO")
    TipoTelefone tipo;

    @ManyToOne
    @JoinColumn(name = "FK_TELEFONE_CLIENTE", foreignKey = @ForeignKey(name = "FK_CONSTRAINT_TELEFONE_CLIENTE"))
    Cliente cliente;

    public Telefone() {
        super();
    }

    public Telefone update(TelefoneDTO telefoneDTO) {
        this.telefone = telefoneDTO.getTelefone();
        this.tipo = telefoneDTO.getTipo();
        return this;
    }

    public long getId() {
        return id;
    }

    public Telefone setId(long id) {
        this.id = id;
        return this;
    }

    public String getTelefone() {
        return telefone;
    }

    public Telefone setTelefone(String telefone) {
        this.telefone = telefone;
        return this;
    }

    public TipoTelefone getTipo() {
        return tipo;
    }

    public Telefone setTipo(TipoTelefone tipo) {
        this.tipo = tipo;
        return this;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public Telefone setCliente(Cliente cliente) {
        this.cliente = cliente;
        return this;
    }

}
