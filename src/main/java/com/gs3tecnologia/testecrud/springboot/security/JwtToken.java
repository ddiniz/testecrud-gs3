package com.gs3tecnologia.testecrud.springboot.security;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gs3tecnologia.testecrud.springboot.entities.Usuario;
import com.gs3tecnologia.testecrud.springboot.enums.Permissao;
import com.gs3tecnologia.testecrud.springboot.utils.exceptions.GS3Exception;
import com.gs3tecnologia.testecrud.springboot.utils.exceptions.rest.ErroJWTDesconhecidoException;
import com.gs3tecnologia.testecrud.springboot.utils.exceptions.rest.TokenUsuarioNullException;

import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtToken {
    static final String CHAVE = "UXMmOWTeoAsB6NEDRXlT44gMHv0cHBPrB63fyzTXW5ufBvT0xofOlvZkXYUbp5";
    static final String TOKEN_PREFIX = "Bearer";

    public static String addAuthentication(HttpServletResponse response, Usuario usuario) {
        Date dataExpires = new Date(Instant.now().plus(30, ChronoUnit.DAYS).toEpochMilli());
        String JWT = Jwts.builder()
                         .setSubject(usuario.getUsername())
                         .setExpiration(dataExpires)
                         .claim("permissao", usuario.getPermissao())
                         .signWith(SignatureAlgorithm.HS512, CHAVE)
                         .compact();
        String token = TOKEN_PREFIX + " " + JWT;
        response.addHeader(HttpHeaders.AUTHORIZATION, token);
        return JWT;
    }

    public static Authentication getAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws GS3Exception {
        String token = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (token == null)
            return null;
        try {
            String dados = Jwts.parser()
                               .setSigningKey(CHAVE)
                               .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                               .getBody()
                               .getSubject();
            if (dados == null)
                throw new TokenUsuarioNullException();
            return new UsernamePasswordAuthenticationToken(dados, null, Collections.emptyList());
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
            throw new ErroJWTDesconhecidoException();
        }
    }

    public static Permissao getPermissaoUsuario(HttpServletRequest request) throws GS3Exception {
        String token = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (token == null)
            return null;
        String aux = Jwts.parser()
                         .setSigningKey(CHAVE)
                         .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                         .getBody()
                         .get("permissao", String.class);
        return Permissao.valueOf(aux);
    }

}