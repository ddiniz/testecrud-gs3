package com.gs3tecnologia.testecrud.springboot.security.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.gs3tecnologia.testecrud.springboot.security.JwtToken;
import com.gs3tecnologia.testecrud.springboot.utils.exceptions.GS3Exception;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

public class JwtAuthFilter extends GenericFilterBean {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        try {
            Authentication authentication = JwtToken.getAuthentication((HttpServletRequest) request,
                    (HttpServletResponse) response);
            SecurityContextHolder.getContext().setAuthentication(authentication);
        } catch (GS3Exception e) {
            e.printStackTrace();
        }
        filterChain.doFilter(request, response);
    }

}