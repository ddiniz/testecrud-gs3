package com.gs3tecnologia.testecrud.springboot.security;

import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;

public class Encryptor {
    public static void main(String[] args) {
        System.out.println(Encryptor.encrypt("123456"));
    }

    public static String encrypt(String dados) {
        SCryptPasswordEncoder encoder = new SCryptPasswordEncoder();
        return encoder.encode(dados);
    }

    public static boolean compare(String dadosRaw, String dadosEncriptados) {
        SCryptPasswordEncoder encoder = new SCryptPasswordEncoder();
        return encoder.matches(dadosRaw, dadosEncriptados);
    }

}
