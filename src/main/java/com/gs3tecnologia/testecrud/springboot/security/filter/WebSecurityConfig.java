package com.gs3tecnologia.testecrud.springboot.security.filter;

import java.util.Arrays;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.cors()
                    .and()
                    .csrf()
                    .disable()
                    .authorizeRequests()
                    .antMatchers(HttpMethod.OPTIONS, "/auth/login")
                    .permitAll()
                    .antMatchers(HttpMethod.POST, "/auth/login")
                    .permitAll()
                    .antMatchers(HttpMethod.GET, "/auth/hello")
                    .permitAll()
                    .antMatchers(HttpMethod.GET, "/cliente/hello")
                    .permitAll()
                    .antMatchers(HttpMethod.GET, "/usuario/hello")
                    .permitAll()
                    .antMatchers(HttpMethod.GET, "/cep")
                    .permitAll()

                    .anyRequest()
                    .authenticated()
                    .and()
                    .addFilterBefore(new JwtAuthFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("http://localhost:3000"));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS", "HEAD"));
        configuration.setAllowedHeaders(Arrays.asList("X-Requested-With", "Authorization", "Accept-Version",
                "Content-MD5", "CSRF-Token", "Content-Type"));
        configuration.setAllowCredentials(true);
        configuration.setExposedHeaders(Arrays.asList("Content-type", "Content-Disposition", "X-Suggested-Filename"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

}