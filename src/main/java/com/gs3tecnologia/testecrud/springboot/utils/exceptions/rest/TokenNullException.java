package com.gs3tecnologia.testecrud.springboot.utils.exceptions.rest;

import com.gs3tecnologia.testecrud.springboot.utils.exceptions.GS3Exception;

import org.springframework.http.HttpStatus;

public class TokenNullException extends GS3Exception {
    private static final long serialVersionUID = 1L;

    public TokenNullException() {
        super("Token nulo", HttpStatus.EXPECTATION_FAILED);
    }

}