package com.gs3tecnologia.testecrud.springboot.utils.exceptions.rest;

import com.gs3tecnologia.testecrud.springboot.utils.exceptions.GS3Exception;

import org.springframework.http.HttpStatus;

public class UsuarioInexistenteException extends GS3Exception {
    private static final long serialVersionUID = 1L;

    public UsuarioInexistenteException() {
        super("Usuário inexistente", HttpStatus.EXPECTATION_FAILED);
    }

}
