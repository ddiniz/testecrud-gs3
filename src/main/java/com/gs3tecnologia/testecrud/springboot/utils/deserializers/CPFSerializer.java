package com.gs3tecnologia.testecrud.springboot.utils.deserializers;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class CPFSerializer extends JsonSerializer<String> {

	@Override
	public void serialize(String cnpj, JsonGenerator jg, SerializerProvider sp)
			throws IOException, JsonProcessingException {
		jg.writeString(addSimbolos(cnpj));
	}

	public String addSimbolos(String str) {
		if (str == null || str.length() < 11)
			return str;
		StringBuilder sb = new StringBuilder(str);
		sb.insert(3, ".").insert(7, ".").insert(11, "-");
		return sb.toString();
	}

}
