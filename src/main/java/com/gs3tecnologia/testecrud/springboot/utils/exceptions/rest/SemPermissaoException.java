package com.gs3tecnologia.testecrud.springboot.utils.exceptions.rest;

import com.gs3tecnologia.testecrud.springboot.utils.exceptions.GS3Exception;

import org.springframework.http.HttpStatus;

public class SemPermissaoException extends GS3Exception {
    private static final long serialVersionUID = 1L;

    public SemPermissaoException() {
        super("Sem permissão", HttpStatus.UNAUTHORIZED);
    }

}