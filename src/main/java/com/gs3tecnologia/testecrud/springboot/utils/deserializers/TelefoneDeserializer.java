package com.gs3tecnologia.testecrud.springboot.utils.deserializers;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.node.TextNode;

public class TelefoneDeserializer extends JsonDeserializer<String> {

	@Override
	public String deserialize(JsonParser jp, DeserializationContext dc) throws IOException, JsonProcessingException {
		ObjectCodec codec = jp.getCodec();
		TextNode node = (TextNode) codec.readTree(jp);
		return node.textValue().replace("(", "").replace(")", "").replace("-", "").replace(" ", "");
	}

}
