package com.gs3tecnologia.testecrud.springboot.utils.exceptions.rest;

import com.gs3tecnologia.testecrud.springboot.utils.exceptions.GS3Exception;

import org.springframework.http.HttpStatus;

public class ErroJWTDesconhecidoException extends GS3Exception {
    private static final long serialVersionUID = 1L;

    public ErroJWTDesconhecidoException() {
        super("Erro desconhecido ao fazer parse de token", HttpStatus.EXPECTATION_FAILED);
    }

}