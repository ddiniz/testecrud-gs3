package com.gs3tecnologia.testecrud.springboot.utils.deserializers;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class TelefoneSerializer extends JsonSerializer<String> {

	@Override
	public void serialize(String tel, JsonGenerator jg, SerializerProvider sp)
			throws IOException, JsonProcessingException {
		jg.writeString(addSimbolos(tel));
	}

	public String addSimbolos(String str) {
		if (str == null || str.length() < 8)
			return str;
		StringBuilder sb = new StringBuilder(str);
		if (sb.length() > 10) {
			sb.insert(0, "(").insert(3, ")").insert(9, "-");
		} else {
			sb.insert(0, "(").insert(3, ")").insert(8, "-");
		}
		return sb.toString();
	}

}
