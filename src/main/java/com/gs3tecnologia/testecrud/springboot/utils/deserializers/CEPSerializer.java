package com.gs3tecnologia.testecrud.springboot.utils.deserializers;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class CEPSerializer extends JsonSerializer<String> {

	@Override
	public void serialize(String cep, JsonGenerator jg, SerializerProvider sp)
			throws IOException, JsonProcessingException {
		jg.writeString(addSimbolos(cep));
	}

	public String addSimbolos(String str) {
		if (str == null || str.length() < 5)
			return str;
		StringBuilder sb = new StringBuilder(str);
		sb.insert(5, "-");
		return sb.toString();
	}

}
