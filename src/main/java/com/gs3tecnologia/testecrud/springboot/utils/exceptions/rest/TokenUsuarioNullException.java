package com.gs3tecnologia.testecrud.springboot.utils.exceptions.rest;

import com.gs3tecnologia.testecrud.springboot.utils.exceptions.GS3Exception;

import org.springframework.http.HttpStatus;

public class TokenUsuarioNullException extends GS3Exception {
    private static final long serialVersionUID = 1L;

    public TokenUsuarioNullException() {
        super("Usuário nulo dentro do token JWT", HttpStatus.EXPECTATION_FAILED);
    }

}
