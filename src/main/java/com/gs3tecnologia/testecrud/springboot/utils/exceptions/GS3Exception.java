package com.gs3tecnologia.testecrud.springboot.utils.exceptions;

import org.springframework.http.HttpStatus;

public class GS3Exception extends Exception {

    static final long serialVersionUID = 1L;
    HttpStatus status = HttpStatus.BAD_REQUEST;

    public GS3Exception(String msg, HttpStatus status) {
        super(msg);
        this.status = status;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }
}